# repo-tracker

This repository illustrates how one might start building a backup solution
for Gitaly servers.

**This is an idea, not a product. See the [GitLab documentation](https://docs.gitlab.com/ee/raketasks/backup_restore.html) if you want to back up your GitLab server.**

All this does for now is to maintain a Sqlite database which tracks which repositories on the Gitaly server have been modified. An actual backup solution would have a "backup manager", running on the Gitaly server, which watches this database and backs up repositories after they changed.

Some observations about this design:

- Backup metadata lives on the same disk as the repositories. They cannot get out of sync with each other.
- Sqlite allows concurrent access to the database from different processes. This prevents the need for a "server" process that must always be available.
- By using a database (Sqlite) the backup manager can avoid doing file walks to look for repositories that have changed.
- If the local backup manager is temporarily unavailable (e.g. during a deploy) we can still accept pushes and taint repositories. The Sqlite database acts as a durable asynchronous communication channel.
- By using a database that is local to the Gitaly server and its repository disk, this approach scales naturally when you have many Gitaly servers.

## Installation

Clone the repository into `/opt/repo-tracker` on an Omnibus GitLab
installation.

Next, run the following commands as root:

```shell
(
set -e

# Install sqlite3
apt-get update -qq
apt-get install -y sqlite3

# Configure Gitaly to use global custom hooks
hook_dir=/var/opt/gitlab/gitaly/custom_hooks
cat >>/etc/gitlab/gitlab.rb <<EOF
gitaly['custom_hooks_dir'] = '$hook_dir'
EOF
gitlab-ctl reconfigure

# Create shim that calls into repo-tracker
post_receive_dir=$hook_dir/post-receive.d
mkdir -p $post_receive_dir
hook=$post_receive_dir/taint
cat >$hook <<'EOF'
#!/bin/sh
set -e
export PATH=/opt/gitlab/embedded/bin:$PATH
/opt/repo-tracker/hooks/post-receive /var/opt/gitlab/git-data/repositories $(pwd)
EOF
chmod +x $hook
)
```

## Try it out

After completing installation, push some commits to your GitLab or edit
a file in a repository using the web editor.

Afterwards, you can see that the repository has been marked as tainted:

```shell
sqlite3 /var/opt/gitlab/git-data/repositories/backup.db 'select * from repositories;'
```

## Where next

- Find or create a tool that can backup a local Git repository to object storage
- Create a "backup manager" that feeds of the Sqlite database created by this hook

